;; -*- lisp -*-

(defpackage :it.bese.ucw.presentations.system
  (:use :common-lisp :asdf))

(in-package :it.bese.ucw.presentations.system)

(defsystem :ucw-presentations
  :description "UCW Presentations - An advanced CRUD library for UCW"
  :licence "BSD (sans advertising clause)"
  :components
  ((:module :src
    :components ((:file "packages")
    		 (:file "aux-slots-mixin" :depends-on ("packages"))
                 (:file "form" :depends-on ("ie" "indirect-value-class" "aux-slots-mixin"))
                 (:file "ie" :depends-on ("packages"))
                 (:file "indirect-value-class")
                 (:file "presentations")
                 (:file "search-criteria" :depends-on ("form" "time-element"))
                 (:file "time-element" :depends-on ("form"))
                 (:file "wall-time" :depends-on ("packages")))))
  :depends-on (:ucw :iterate :parenscript :cl-ppcre))
