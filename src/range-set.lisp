(in-package :it.bese.ucw.presentations)

(defclass range-cursor ()
  ((data :accessor range-cursor.data
                 :initarg :data
                 :documentation
                 "Data to range.")
   (chunk-size :accessor range-cursor.chunk-size
               :initarg :chunk-size
               :type integer
               :initform 20
               :documentation
               "Maximum number of elements in one chunk.")
   (offset :accessor range-cursor.offset
           :type integer
           :initarg :offset
           :initform 0
           :documentation
           "Current offset in chunks.")
   (recent-fetch-size :reader range-cursor.recent-fetch-size
                     :initform nil
                     :documentation
                     "Number of recently retrieved data elements."))
  (:documentation "Read chunks of elements from data at arbitrary offset."))

(defgeneric range-cursor.data-size (range-cursor data)
  (:method ((rc range-cursor) (rd sequence))
    (length rd))
  (:documentation "Return total number of elements in the DATA."))

(defgeneric range-cursor.fetch-data-chunk (range-cursor data offset length)
  (:method ((rc range-cursor) (data sequence) (offset integer) (length integer))
    (subseq data offset (+ offset length)))
  (:documentation "Read LENGTH elements from DATA starting from
OFFSET(excluding OFFSET element).
Return a list of elements."))

(defgeneric range-cursor.delete-data-item (range-cursor data item index)
  (:method ((rc range-cursor) (data sequence) item (index integer))
    (declare (ignore item))
    (when (<= index (length data))
      (setf (range-cursor.data rc)
            (delete-if (constantly t) data :start (1- index) :count 1))
      t))
  (:documentation "Delete ITEM with INDEX from cursor related DATA."))

(defgeneric range-cursor.add-data-item (range-cursor data item)
  (:method ((rc range-cursor) (data sequence) item)
    (push item (range-cursor.data rc))
    t)
  (:documentation "Add ITEM with INDEX to cursor related DATA."))

(defgeneric range-cursor.range-size (range-cursor)
  (:documentation "Return the size of the set in chunks."))

(defmethod range-cursor.range-size ((rc range-cursor))
  (with-slots (data chunk-size) rc
    (multiple-value-bind (q rem)
        (floor (range-cursor.data-size rc data) chunk-size)
      (if (zerop rem) q (1+ q)))))

(defmethod range-cursor.have-previous-p ((rc range-cursor))
  (> (range-cursor.offset rc) 0))

(defmethod range-cursor.have-next-p ((rc range-cursor))
  (aif (range-cursor.recent-fetch-size rc)
       (> it (range-cursor.chunk-size rc))
       t ; no reads yet
       ))

(defgeneric range-cursor.fetch-chunk (range-cursor op)
  (:documentation "Retrieve chunk from the data.
Returns a list of elements."))

(defmethod range-cursor.fetch-chunk :before ((rc range-cursor) op)
  (with-slots (offset recent-fetch-size) rc
    (setf offset
          (ecase op
            (:first 0)
            (:last (max (1- (range-cursor.range-size rc)) 0))
            (:next (if (and recent-fetch-size (range-cursor.have-next-p rc))
                       (1+ offset)
                       offset))
            (:previous (if (range-cursor.have-previous-p rc)
                           (1- offset)
                           offset))
            (:current (max 0 offset))))))

(defmethod range-cursor.fetch-chunk ((rc range-cursor) op)
  (declare (ignore op))
  (with-slots (chunk-size recent-fetch-size offset data) rc
    (let ((items (range-cursor.fetch-data-chunk rc data (* offset chunk-size) (1+ chunk-size))))
      (setf recent-fetch-size (length items))
      (if (> recent-fetch-size chunk-size)
          (butlast items)
          items))))

(defmethod range-cursor.delete-item ((rc range-cursor) item (index integer))
  "Delete ITEM with INDEX from cursor related DATA."
  (range-cursor.delete-data-item rc (range-cursor.data rc) item index))

(defmethod range-cursor.add-item ((rc range-cursor) item)
  "Add ITEM to cursor related DATA."
  (range-cursor.add-data-item rc (range-cursor.data rc) item))

