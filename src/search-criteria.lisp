(in-package :it.bese.ucw.presentations)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (define-ie-method applicable-criteria nconc ((e (ie-type t)))
                    '())

  (define-ie-method negative-criteria ((e (ie-type t)))
                    nil)

  (define-ie-generic criteria-label (criteria interface-element)
    (:method ((c (eql :not)) (e (ie-type t)))
      "not")
    (:method ((c (eql :=)) (e (ie-type t)))
      "equal to")
    (:method ((c (eql :>)) (e (ie-type t)))
      "greater than")
    (:method ((c (eql :<)) (e (ie-type t)))
      "less than")
    (:method ((c (eql :contains)) (e (ie-type t)))
      "contains")
    (:method ((c (eql :starts-with)) (e (ie-type t)))
      "starts with")
    (:method ((c (eql :ends-with)) (e (ie-type t)))
      "ends with")
    (:method ((c (eql :>)) (e (ie-type time)))
      "after")
    (:method ((c (eql :<)) (e (ie-type time)))
      "before"))

  (define-ie-generic make-criteria-attribute-value (element)
    (:method ((e (ie-type t)))
      (let ((ce (clone-element e :constraints nil :direct-value nil)))
        (setf (editablep ce) t)
        ce)))

  (define-ie-method negative-criteria ((e (ie-type interface-element)))
                    :not)

  (define-ie-method applicable-criteria nconc ((e (ie-type text)))
                    '(:= :contains :starts-with :ends-with))

  (define-ie-method applicable-criteria nconc ((e (ie-type number)))
                    '(:= :> :<))

  (define-ie-method applicable-criteria nconc ((e (ie-type time)))
                    '(:= :> :<))

  (define-ie-method applicable-criteria nconc ((e (ie-type selector)))
                    '(:=))

  (define-ie-method applicable-criteria nconc ((e (ie-type checkbox)))
                    '(:=))

  (define-ie-method negative-criteria ((e (ie-type checkbox)))
                    nil)
  )

(defclass alist-selector (select-element)
  ()
  (:metaclass standard-component-class)
  (:default-initargs
    :key #'car
    :output-format #'(lambda (&rest args) (cdr (car args)))))

(defclass search-criteria (composite-element)
  ((attribute :accessor attribute
              :initarg :attribute
              :documentation "Attribute name criteria is applied to.")
   (negativep :accessor negativep
              :component (alist-selector)
              :documentation
              "Is criteria negative.")
   (criteria :accessor criteria
             :component (alist-selector)
             :documentation
             "Criteria to add into a query.")
   (attribute-value :accessor attribute-value
                   :initarg :attribute-value
                   :documentation
                   "Value of attribute."))
  (:metaclass standard-component-class)
  (:default-initargs    :slot-view (make-instance 'ieview :labelp nil))
  (:documentation "Search criteria."))

(defmethod shared-initialize :after ((sc search-criteria) slot-names
                                     &key &allow-other-keys)
  (declare (ignore slot-names))
  (with-slots (attribute-value criteria negativep label slot-view) sc
    (setf label (label attribute-value))

    (let ((ac (applicable-criteria attribute-value)))
      (setf (options criteria)
          (mapcar #'(lambda (c) (cons c (criteria-label c attribute-value)))
                  ac))
      (case (length ac)
        (0 (setf (enabledp sc) nil))            ; no criteria, disable it
        (1 (setf (lisp-value criteria) (car ac) ; single criteria
                 (editablep criteria) nil))     ; fix value
        (otherwise t)))

    (aif (negative-criteria attribute-value)
         (setf (options negativep)
               (acons nil "---"
                      (acons it (criteria-label it attribute-value) nil)))
         (setf (enabledp negativep) nil))))

(defmethod query-value ((sc search-criteria))
  (let ((lv (lisp-value sc))
        negative
        criteria
        attr-value)
    (setf (values negative criteria attr-value)
          (values-list (mapcar #'(lambda (sn) (cdr (assoc sn lv)))
                               '(negativep criteria attribute-value))))
    (when criteria
      (if (and (enabledp (negativep sc)) negative)
          (list negative (list criteria attr-value))
          (list criteria attr-value)))))

(defmethod (setf query-value) (new (sc search-criteria))
  (when new
    (let (negative criteria attr-value)
      (destructuring-bind (op op-value) new
        (awhen (eq op (negative-criteria (attribute-value sc)))
          (setf negative (negative-criteria (attribute-value sc))
                (values op op-value) (values-list op-value)))
        (setf criteria op
              attr-value op-value))
      (setf (lisp-value sc)
            (pairlis '(negativep criteria attribute-value)
                     (list negative criteria attr-value))))))

(defmethod accept ((view composite-view) (sc search-criteria))
  (with-slots (attribute-value) sc
    (setf (view attribute-value) view)
    (render attribute-value)))

(defclass search-attribute-selector (select-element)
  ()
  (:metaclass standard-component-class)
  (:default-initargs
     :label "Add to query"
     :output-format #'(lambda (v e)
                        (declare (ignore e))
                        (label (cdr v)))))

(defmethod render-element ((view composite-view) (e search-attribute-selector))
  (accept view e)
  (<ucw:input :type "submit"
              :value (label e)
              :action (submit e)))

(defmethod attribute-value-element ((e search-attribute-selector) attr
                                    &key (errorp t))
  (aif (find-if #'(lambda (v) (eql v attr)) (options e) :key #'car)
     (cdr it)
     (when errorp
       (error "couldn't find query attribute ~a" attr))))

(defaction submit ((e search-attribute-selector))
  (setf (lisp-value e) (read-client-value e))
  (aif (lisp-value e)
       (add-search-attribute (parent e) (car it) (cdr it)))
  t)
 
(define-ie-type (search search-element) ((ie-type auxslots-form))
  ((attribute-selector :accessor attribute-selector
                       :component (search-attribute-selector)))
  (:metaclass standard-component-class)
  (:documentation "Accepts as ATTRIBUTES initarg
alist of search attributes (ATTRIBUTE-NAME . INTERFACE-ELEMENT)
or interface element to copy slots from as attributes.
Initial query can be supplied as QUERY-VALUE initarg.
Accessor  QUERY-VALUE method accepts/return query in the following format:
(ATTRIBUTE-NAME ATTRIBUTE-QUERY)*
ATTRIBUTE-NAME -- symbol
ATTRIBUTE-QUERY -- NEGATIVE-EXPR | EXPR
NEGATIVE-EXPR -- (:not EXPR)
EXPR -- (OP VALUE)
OP -- keyword from (APPLICABLE-CRITERIA interface-element)
VALUE -- term acceptable by interface-element."))

(defmethod shared-initialize :after ((se search-element) slot-names
                                     &key attributes query-value
                                     &allow-other-keys)
  (declare (ignore slot-names))
  (init-attribute-selector se attributes)
  (setf (query-value se) query-value))

(defmethod init-attribute-selector ((se search-element) (attributes list))
  (setf (options (attribute-selector se)) attributes))

(defmethod init-attribute-selector ((se search-element) (e composite-element))
  (init-attribute-selector se
                           (mapcar #'(lambda (sn) (cons sn (slot-value e sn)))
                                   (slot-elements e :test #'enabledp))))

(defmethod search-criteria-slots ((se search-element))
  (let ((criteria-class (find-class 'search-criteria)))
    (slot-elements se :test #'(lambda (s)
                                (and (enabledp s)
                                     (subtypep (class-of s) criteria-class))))))

(defmethod add-search-attribute ((se search-element) attr attr-value)
  (let ((slot-name (gensym)))
    (setf (slot-value se slot-name)
          (make-instance 
           'search-criteria
           :attribute attr
           :attribute-value (make-criteria-attribute-value attr-value)))
    slot-name))

(defmethod query-value ((se search-element))
  (mapcar #'(lambda (sn)
              (let ((slot (slot-value se sn)))
                (list (attribute slot) (query-value slot))))
          (search-criteria-slots se)))

(defmethod (setf query-value) (new (se search-element))
  (dolist (slot (search-criteria-slots se))
    (slot-makunbound se slot))
  (iterate
    (with as = (attribute-selector se))
    (for (attr attr-query) in new)
    (for ave = (attribute-value-element as attr))
    (for slot-name = (add-search-attribute se attr ave))
    (setf (query-value (slot-value se slot-name)) attr-query)))

(defaction delete-search-attribute ((se search-element) slot-name)
  (slot-makunbound se slot-name)
  t)

(defmethod render-slot ((view composite-view) (sc search-element) (slot-name (eql 'attribute-selector)))
  (<:td :colspan 5 :align "center"
        (render (slot-value sc slot-name))))

(defmethod render-slot ((view composite-view) (sc search-element) (slot-name symbol))
  (call-next-method)
  (<:td :class "ucw-composite-slot-value"
        (<ucw:a :action (delete-search-attribute sc slot-name)
                (<:as-html "Eliminate"))))

(defmethod render-slot-value-box ((view composite-view) (sc search-element) (slot search-criteria))
  (declare (ignore view sc))
  (<:td (render (negativep slot)))
  (<:td (render (criteria slot)))
  (<:td (render slot)))
