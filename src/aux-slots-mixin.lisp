(in-package :it.bese.ucw.presentations)

(defclass aux-slots-mixin ()
   ((%aux-slots
     :initform (make-hash-table :test #'eq))
    (%aux-slots-order :reader aux-slots
                      :initform nil)))

(defmethod slot-missing
            ((class t)
             (obj aux-slots-mixin)
             slot-name operation &optional new-value)
  (with-slots (%aux-slots %aux-slots-order)
      obj
   (ecase operation
     (slot-value
      (multiple-value-bind (value present-p)
                          (gethash slot-name %aux-slots)
        (if present-p value
            (slot-unbound class obj slot-name))))

     (setf
      (unless (nth-value 1 (gethash slot-name %aux-slots))
        (setf %aux-slots-order (nconc %aux-slots-order (list slot-name))))
      (setf (gethash slot-name %aux-slots) new-value))

     (slot-boundp
      (nth-value 1 (gethash slot-name %aux-slots)))

     (slot-makunbound
      (setf %aux-slots-order (delete slot-name %aux-slots-order))
      (remhash slot-name %aux-slots)))))
